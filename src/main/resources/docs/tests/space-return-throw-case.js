//#Patterns: space-return-throw-case


//#Info: space-return-throw-case
throw{a:0}                 
//#Info: space-return-throw-case
function f(){ return-a; }
//#Info: space-return-throw-case
switch(a){ case'a': break; }


throw {a: 0};
function f(){ return -a; }
switch(a){ case 'a': break; }
