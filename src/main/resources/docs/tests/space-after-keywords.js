//#Patterns: space-after-keywords

//#Info: space-after-keywords
if(a) {}
//#Info: space-after-keywords
if (a) {} else{}
//#Info: space-after-keywords
do{} while (a);

if (a) {}
if (a) {} else {}
