//#Patterns: space-before-keywords


if (foo) {
    // ...
//#Info: space-before-keywords
}else {}
//#Info: space-before-keywords
try {}finally {}

if (foo) {
    // ...
} else {}

try {} finally {}
